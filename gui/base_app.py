# coding: utf-8
import logging
import Tkinter as tk
from tkFileDialog import askopenfilename, asksaveasfilename
from tkMessageBox import showinfo, showerror, showwarning
from ttk import Button, Label

from converter import KrasInformDBConverter


class App(tk.Frame):
    title = 'Simple KrasInform DB converter'
    source_filepath = None
    output_filepath = None

    def __init__(self):
        tk.Frame.__init__(self)
        self.master.wm_title(self.title)
        self.grid()
        self.create_widgets()

    def fileopen(self):
        self.source_filepath = askopenfilename(parent=self)
        if self.source_filepath:
            self.label_fileopen.set(self.source_filepath)
            self.label_filesave.set('Ready to convert')
            self.update_idletasks()

    def convert(self):
        if self.source_filepath:
            self.output_filepath = asksaveasfilename(parent=self)
            try:
                KrasInformDBConverter(self.source_filepath, self.output_filepath).create_txt()
                showinfo('Done', 'You result file in\n %s' % self.output_filepath)
                self.label_filesave.set('Done, output file is: %s' % self.output_filepath)
                self.update_idletasks()

            except Exception as e:
                logging.exception(e)
                showerror('Error', 'Some error occured \n Please, email developer: lobziiko.o@gmail.com.\n'
                                   'Do not forget attach simple_converter.log file!!!')
        else:
            showwarning('Warning', 'Please, specify source file!')

    def create_widgets(self):
        self.button_fileopen = Button(self, text='Open', command=self.fileopen)
        self.label_fileopen = tk.StringVar(value='Please open source excel file')
        label_fileopen_widget = Label(self, textvariable=self.label_fileopen)

        self.button_convert = Button(self, text='Convert', command=self.convert)
        self.label_filesave = tk.StringVar(value='Source file not specified')
        label_filesave_widget = Label(self, textvariable=self.label_filesave)

        self.button_fileopen.grid(row=1, column=1)
        label_fileopen_widget.grid(row=1, column=2)
        self.button_convert.grid(row=2, column=1)
        label_filesave_widget.grid(row=2, column=2)


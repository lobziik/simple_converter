# coding: utf-8
import sys
from cx_Freeze import setup, Executable


options = {
    'build_exe': {
        'compressed': True,
        'includes': [
            'gui',
            'converter',
            'Tkinter',
            'tablib',
            'simplejson'
        ],
        "excludes": ["unittests"],
        'path': sys.path + ['modules']
    }
}

base = None
if sys.platform == "win32":
    base = "Win32GUI"

executables = [
    Executable('main.py', base=base)
]

setup(
    name='simple_converter',
    version='0.1',
    description='Sample converter for tkt',
    executables=executables,
    options=options
)

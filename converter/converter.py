# coding: utf-8
import logging

from tablib import import_set, Dataset


class KrasInformDBConverter(object):
    """
    ATTENTION - Its prototype, not suitable for production usage!!!

    Usage: KrasInformDBConverter(path_to_excel_file, path_to_result_file)

    Methods:
        - convert_base: return list in format ('account_number', 'city', 'house_number', 'flat_number', 'debt')
            if row contains credit column - debt => -credit
        - create_txt: create file that was specified in path_to_result_file argument, according to format in format
            attribute, need completion
    """
    city = u'Красноярск'
    street_abbreviation = u'ул.'
    house_number_abbreviation = u'д.'
    room_number_abbreviation = u'кв.'
    encoding = 'cp1251'
    format = 'csv'

    def __init__(self, filepath, result_file):
        self.filepath = filepath
        self.result_filename = result_file
        self.dataset = import_set(filepath)

    def _rm_abbreviations(self, string):
        return string.replace(self.street_abbreviation, '')\
            .replace(self.house_number_abbreviation, '').replace(self.room_number_abbreviation, '').strip()

    def _convert_address(self, addr_string, house_number):
        values_list = addr_string.split(',')
        converted_addr_row = [
            self.city,
            self._rm_abbreviations(values_list[0]),
            self._rm_abbreviations(values_list[1]),
            #self._rm_abbreviations(values_list[2]),
            unicode(house_number)
        ]
        return [value.encode(self.encoding) for value in converted_addr_row]

    def convert_base(self):
        result = []
        for personal_account in self.dataset.dict:
            account_number, source_addr, house_number, debt, credit = (column for column in personal_account.itervalues())
            try:
                converted_row = [account_number] + self._convert_address(source_addr, house_number)
            except IndexError:
                logging.debug('Waste string')
                continue
            if debt:
                converted_row += [debt]
            elif credit:
                converted_row += [-credit]
            else:
                continue
            result.append(converted_row)
        return result

    def create_txt(self):
        data = self.convert_base()
        dataset = Dataset(*data)
        with open(self.result_filename, 'w') as fd:
            output = getattr(dataset, self.format)
            if self.format == 'csv':
                # speed workaround about tablib delimeters, need use csv module from python std library
                output = output.replace(',', ';')
            fd.write(output)
        logging.info('File created')

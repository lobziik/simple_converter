# coding: utf-8
import logging

from gui import App


if __name__ == "__main__":
    logging.basicConfig(filename='simple_converter.log', level=logging.DEBUG)
    logging.info('App started')
    App().mainloop()
    logging.info('Done')
